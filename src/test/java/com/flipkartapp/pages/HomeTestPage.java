package com.flipkartapp.pages;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "homePage.searchWidgets")
	private QAFWebElement homePageSearchWidgets;
	
	
	@FindBy(locator = "homePage.appleIpad.links")
	private QAFWebElement homePageAppleIpadLinks;

	@FindBy(locator = "homePage.searchTextbox")
	private QAFWebElement homePageSearchTextbox;
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getHomePageSearchWidgets() {
		return homePageSearchWidgets;
	}

	public QAFWebElement getHomePageAppleIpadLinks() {
		return homePageAppleIpadLinks;
	}
	
	public QAFWebElement getHomePageSearchTextbox() {
		return homePageSearchTextbox;
	}

	
	@QAFTestStep(description = "user search for {0} in category")
	public void searchItemInCategory(String item) {
		System.out.println("inside search method *******************************");
		getHomePageSearchWidgets().click();
		System.out.println("item "+item);
		homePageSearchTextbox.waitForPresent(3000);
		homePageSearchTextbox.sendKeys(item);
	}
	
	@QAFTestStep(description = "user clicks on {0} link from subcategory")
	public void selectRequiredSubCategory(String subCategory) {
		QAFExtendedWebElement qafExtendedWebElement = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("homePage.appleIpad.links"), subCategory));
		qafExtendedWebElement.waitForPresent(3000);
		qafExtendedWebElement.click();
	}

	
}
