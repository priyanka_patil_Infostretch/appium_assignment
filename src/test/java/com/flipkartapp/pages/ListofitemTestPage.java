package com.flipkartapp.pages;


import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.flipkartapp.bean.ProductDetails;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

public class ListofitemTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	@FindBy(locator = "listOfItems.iPadList")
	private List<QAFWebElement> listOfItemsIPadList;
	
	@FindBy(locator = "ListOfitems.iPadNames")
	private List<QAFWebElement> listOfItemsIPadNames;


	@FindBy(locator = "ListOfitems.iPadColors")
	private List<QAFWebElement> listOfItemsIPadColors;
	
	@FindBy(locator = "ListOfitems.iPadPrice")
	private List<QAFWebElement> listOfItemsIPadPrice;

	@FindBy(locator = "ListOfItems.allProducts")
	private QAFWebElement listOfAllProducts;
	
	@FindBy(locator = "ListOfItems.sortIcon")
	private QAFWebElement sortIcon;
	
	@FindBy(locator = "ListOfItems.sortingOrder")
	private QAFWebElement sortingOrder;
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public List<QAFWebElement> getListOfItemsIPadList() {
		return listOfItemsIPadList;
	}
	
	public List<QAFWebElement> getListOfItemsIPadNames() {
		return listOfItemsIPadNames;
	}
	
	public List<QAFWebElement> getListOfItemsIPadColors() {
		return listOfItemsIPadColors;
	}
	
	public List<QAFWebElement> getListOfItemsIPadPrice() {
		return listOfItemsIPadPrice;
	}
	
	public QAFWebElement getListOfAllProducts() {
		return listOfAllProducts;
	}
	
	public QAFWebElement getSortIcon() {
		return sortIcon;
	}
	
	public QAFWebElement getSortingOrder() {
		return sortingOrder;
	}
	@QAFTestStep(description = "Select item from list")
	public void getitemList(){
		System.out.println("insode method *****************************");
		ProductDetails.product_price = listOfItemsIPadPrice.get(0).getText();
		System.out.println("ProductDetails.product_price "+ProductDetails.product_price);
		
		ProductDetails.product_name = listOfItemsIPadNames.get(0).getText();
		System.out.println("ProductDetails.product_name "+ProductDetails.product_name);
		
		ProductDetails.product_color = listOfItemsIPadColors.get(0).getText();
		System.out.println("ProductDetails.product_color "+ProductDetails.product_color);
		
		 listOfItemsIPadNames.get(0).waitForPresent(3000);
		 listOfItemsIPadNames.get(0).click();
		
	}
	
	
	@QAFTestStep(description = "make a list of all products")
	public void getListOfItems() {
		List<WebElement> product_list = listOfAllProducts.findElements(By.xpath("//android.widget.LinearLayout[@content-desc='product_list']"));
		for(int i=0;i<2;i++) {
			System.out.println("*******Product Details*******");
			System.out.println("Product Price : "+product_list.get(i).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText());
			System.out.println("Product Name : "+product_list.get(i).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[1]")).getText());
			System.out.println("Product Color :"+product_list.get(i).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[2]")).getText());
			System.out.println("**********************/n");
		}
		
	try {
		
		int startx = listOfItemsIPadNames.get(0).getLocation().getX();
		int starty = listOfItemsIPadNames.get(0).getLocation().getY();
		int endx = listOfItemsIPadNames.get(1).getLocation().getX();
		int endy = listOfItemsIPadNames.get(1).getLocation().getY();
		
		for(int i=0;i<20;i++) {
			new TouchAction((AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver()).press(endx+200, endy+200).waitAction()
			.moveTo(startx, starty).release().perform();
			 Thread.sleep(2000);
			 product_list = listOfAllProducts.findElements(By.xpath("//android.widget.LinearLayout[@content-desc='product_list']"));
			 System.out.println("*******Product Details*******");
			 System.out.println("Product Price : "+product_list.get(2).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText());
			 System.out.println("Product Name : "+product_list.get(2).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[1]")).getText());
			 System.out.println("Product Color :"+product_list.get(2).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[2]")).getText());
			 System.out.println("**********************/n");
		   }	
		
	}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@QAFTestStep(description = "user choose lowest price product from {0}")
	public void chooseLowestPriceProduct(ArrayList<WebElement> productList) {
		System.out.println("ProductList lowest "+productList.size());
		WebElement element = productList.get(1);
		String str_price;
		int price = 0;
		int temp = 0;
		for(int i=1; i<productList.size()-1;i++) {
			System.out.println("inside for loop ********************"+i);
			str_price = productList.get(i).findElement(By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText().replace("Rs.", "");
			System.out.println("str_price "+str_price);
		}
//		for(int i=1; i<productList.size()-1;i++) {
//			
//			
//			str_price = productList.get(i).findElement(By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText().replace("Rs.", "");
//			System.out.println("str_price "+str_price);
//			price = Integer.parseInt(str_price);
//			if(i==1) {
//				temp=price;
//				System.out.println("1st condition temp : "+temp);
//				System.out.println("1st condition price : "+price);
//			}
//			System.out.println("***************temp : "+temp);
//			System.out.println("***************price : "+price);
//			if(price < temp || price == temp) {
//				temp = price;
//				element = productList.get(i);
//				System.out.println("inside if "+i);
//				ProductDetails.product_price = productList.get(i).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText();
//				System.out.println("ProductDetails.product_price "+ProductDetails.product_price);
//				
//				ProductDetails.product_name = productList.get(i).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[1]")).getText();
//				System.out.println("ProductDetails.product_name "+ProductDetails.product_name);
//				
//				ProductDetails.product_color = productList.get(i).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[2]")).getText();
//				System.out.println("ProductDetails.product_color "+ProductDetails.product_color);
//			}
//		}
//		element.click();
	}
	
	@QAFTestStep(description = "get Lowest Price product details")
	public void getLowestPriceProductDetails() {
		
		System.out.println("************************************************************");
		System.out.println("ProductDetails.product_price "+ProductDetails.product_price);
		System.out.println("ProductDetails.product_name "+ProductDetails.product_name);
		System.out.println("ProductDetails.product_color "+ProductDetails.product_color);
		System.out.println("************************************************************");
	}
	
	@QAFTestStep(description = "get a list of all products")
	public ArrayList<WebElement> getListOfProducts() {
		ArrayList<WebElement> productList = new ArrayList<WebElement>();
		List<WebElement> product_list = listOfAllProducts.findElements(By.xpath("//android.widget.LinearLayout[@content-desc='product_list']"));
		productList.add(product_list.get(0));
		productList.add(product_list.get(1));
		
		try {
		int startx = listOfItemsIPadNames.get(0).getLocation().getX();
		int starty = listOfItemsIPadNames.get(0).getLocation().getY();
		int endx = listOfItemsIPadNames.get(1).getLocation().getX();
		int endy = listOfItemsIPadNames.get(1).getLocation().getY();
		for(int i=0;i<20;i++) {
			new TouchAction((AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver()).press(endx+200, endy+200).waitAction()
			.moveTo(startx, starty).release().perform();
			 Thread.sleep(2000);
			 product_list = listOfAllProducts.findElements(By.xpath("//android.widget.LinearLayout[@content-desc='product_list']"));
			 System.out.println("inside get list "+product_list.get(2).findElement(By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText());
			 System.out.println(productList.add(product_list.get(2)));
			 System.out.println("product list size "+ productList.size());
			 System.out.println("productList value "+ productList.get(productList.size()-1).findElement(By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText());
		}	
		System.out.println("ProductList size "+productList.size());
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return productList;
	}
	
	@QAFTestStep(description = "user sort product on basis on price")
	public void sortProduct() {
		try{
			sortIcon.click();
			Thread.sleep(3000);
			sortingOrder.click();
			Thread.sleep(3000);
		}catch (Exception e) {
		System.out.println(e.getMessage());
		}
	}
	
	@QAFTestStep(description = "user click on lowest price product from list")
	public void selectLowestPriceProduct() {
		try {
			List<QAFWebElement> product_list = listOfAllProducts.findElements(By.xpath("//android.widget.LinearLayout[@content-desc='product_list']"));
			
			 ProductDetails.product_price = product_list.get(0).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.TextView[1]")).getText();
			 ProductDetails.product_name  = product_list.get(0).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[1]")).getText();
			 ProductDetails.product_color = product_list.get(0).findElement(By.xpath("//android.widget.LinearLayout//android.widget.RelativeLayout/android.widget.TextView[2]")).getText();
			 
			product_list.get(0).click();
			Thread.sleep(2000);
		}catch (Exception e) {
			System.out.println(e.getMessage());		}
	}
	
	
}
