package com.flipkartapp.pages;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class LoginTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "loginPage.skipBtn")
	private QAFWebElement loginPageSkipBtn;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getLoginPageSkipBtn() {
		return loginPageSkipBtn;
	}

	@QAFTestStep(description = "user skip authentication")
	public void userSkipAuthentication() {
		loginPageSkipBtn.click();
	}

}
