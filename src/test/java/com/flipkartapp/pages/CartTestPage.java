package com.flipkartapp.pages;


import com.flipkartapp.bean.ProductDetails;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class CartTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "cartPage.continueBtn")
	private QAFWebElement cartPageContinueBtn;
	
	@FindBy(locator = "cartPage.productName")
	private QAFWebElement cartPageProductName;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getCartPageContinueBtn() {
		return cartPageContinueBtn;
	}

	@QAFTestStep(description = "Verify the product name in cart details")
	public void verifyProductName() {
		System.out.println(ProductDetails.product_name+"  Product_name");
		QAFExtendedWebElement qafExtendedWebElement = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("cartPage.productName"), ProductDetails.product_name));
	    qafExtendedWebElement.waitForPresent(6000);
		qafExtendedWebElement.verifyVisible();
	}

}
