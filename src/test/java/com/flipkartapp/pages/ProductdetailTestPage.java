package com.flipkartapp.pages;



import org.openqa.selenium.By;
import org.testng.Assert;
import com.flipkartapp.bean.ProductDetails;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ProductdetailTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "productDetails.addToCart.btn")
	private QAFWebElement productDetailsAddToCartBtn;
	
	@FindBy(locator = "productDetails.productSummary")
	private QAFWebElement productDetailsProductSummary;
	
	@FindBy(locator = "productDetails.priceLayout")
	private QAFWebElement productDetailsPriceLayout;

	@FindBy(locator = "productDetails.goToCart.btn")
	private QAFWebElement productDetailsGoToCartBtn;
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getProductDetailsAddToCartBtn() {
		return productDetailsAddToCartBtn;
	}

	public QAFWebElement getProductDetailsProductSummary() {
		return productDetailsProductSummary;
	}

	public QAFWebElement getProductDetailsPriceLayout() {
		return productDetailsPriceLayout;
	}
	
	public QAFWebElement getProductDetailsGoToCartBtn() {
		return productDetailsGoToCartBtn;
	}
	
	@QAFTestStep(description = "verify the product details")
	public void verifyProductDetails(){
		productDetailsProductSummary.waitForPresent(3000);
		String productName = productDetailsProductSummary.findElement(By.xpath("//android.widget.FrameLayout//android.widget.TextView")).getText().trim();
		String productColor = productName.substring(productName.indexOf("(")+1,productName.indexOf(")"));
		productName = productName.substring(0, productName.indexOf("("));
		String productPrice = productDetailsProductSummary.findElement(By.xpath("//android.widget.LinearLayout[1]/android.widget.TextView[2]")).getText().replaceAll(",", "");
		String currency = productDetailsProductSummary.findElement(By.xpath("//android.widget.LinearLayout[1]/android.widget.TextView[1]")).getText();
		if(currency.equals("₹")) {
			productPrice = "Rs."+productPrice;
		}
//		System.out.println("valitor stmnts**************");
//		System.out.println("ProductDetails.product_name "+ProductDetails.product_name);
//		System.out.println("ProductDetails.product_name "+productName);
//		Validator.verifyThat(ProductDetails.product_name, Matchers.equalTo(productName));
//		System.out.println("valitor stmnts 1**************");
//		Validator.verifyThat(ProductDetails.product_price, Matchers.equalTo(productPrice));
//		System.out.println("valitor stmnts 2**************");
//		Validator.verifyThat(ProductDetails.product_color, Matchers.equalTo(productColor));
//		System.out.println("valitor stmnts 3**************");
		
		System.out.println("productName.trim() "+productName.trim());
		System.out.println("ProductDetails.product_name.trim()  "+ProductDetails.product_name.trim());
	Assert.assertEquals(productName.trim(), ProductDetails.product_name.trim(), "Product Name in List do not match with Product Name from Details Page.");
	Assert.assertEquals(productPrice, ProductDetails.product_price, "Product Price in List do not match with Product price from Details Page.");
	Assert.assertEquals(productColor, ProductDetails.product_color, "Product color in List do not match with Product color from Details Page.");
	}
	
	@QAFTestStep(description = "user add product to cart")
	public void addProductToCart(){
		productDetailsAddToCartBtn.waitForPresent(3000);
		productDetailsAddToCartBtn.click();
	}
	
	@QAFTestStep(description = "user click on Go To Cart button")
	public void clickGoToCartButton(){
		productDetailsGoToCartBtn.waitForPresent(3000);
		productDetailsGoToCartBtn.click();
	}
	
}
